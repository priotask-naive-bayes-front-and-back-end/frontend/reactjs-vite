import React, { useEffect, useState } from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import * as bootstrap from "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

const D_Calendar = ({ tasks, isDark }) => {
   const [events, setEvents] = useState([]);

   useEffect(() => {
      if (tasks) {
         const taskEvents = tasks
            .filter((task) => task.isProgress)
            .map((task) => ({
               color: getRandomBlueColor(),
               title: task.taskName,
               start: new Date(task.dateStarted).toISOString(),
               end: new Date(task.deadline).toISOString(),
               textColor: "white",
            }));
         setEvents(taskEvents);
      } else {
         setEvents([]);
      }
   }, [tasks]);

   const getRandomBlueColor = () => {
      const letters = "0123456789ABCDEF";
      let color = "#";
      for (let i = 0; i < 2; i++) {
         color += "0";
         color += "9";
         color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
   };
   const formatEventEndDate = (end) => {
      if (!end) {
         return "Today";
      }
      if (!(end instanceof Date)) {
         end = new Date(end);
      }
      const endIndex = end.toISOString().indexOf("T");
      if (endIndex !== -1) {
         return end.toISOString().substring(0, endIndex);
      }
      return end.toISOString();
   };

   return (
      <div className={`calendar ${isDark ? "dark" : ""}`}>
         <span className="pages-title-predict-calendar ">
            In Progress Calendar
         </span>
         <FullCalendar
            plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
            initialView={"dayGridMonth"}
            headerToolbar={{
               start: "today prev,next",
               center: "title",
               end: "dayGridMonth,timeGridWeek,timeGridDay",
            }}
            height={"80vh"}
            events={events}
            eventDidMount={(info) => {
               return new bootstrap.Popover(info.el, {
                  title: info.event.title,
                  placement: "auto",
                  trigger: "hover",
                  customClass: "popoverStyle",
                  content: `<p>Will end <strong>${
                     info.event.end === null
                        ? `Today`
                        : `at ${formatEventEndDate(info.event.end)} <br/> `
                  }</strong></p>`,
                  html: true,
               });
            }}
         />
      </div>
   );
};

export default D_Calendar;
