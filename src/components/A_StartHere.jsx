import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Table from "react-bootstrap/Table";
import TaskService from "../services/TaskService";
import Swal from "sweetalert2";

const A_StartHere = ({ isDark, setTasks, tasks }) => {
   const navigate = useNavigate();
   let numberCount = 0;
   const [userEmail, setUserEmail] = useState([]);

   useEffect(() => {
      const fetchData = async () => {
         try {
            const onlineUsersResponse = await TaskService.getOnlineUsers();
            const onlineUsers = onlineUsersResponse.data.filter(
               (user) => user.email
            );
            const userEmails = onlineUsers.map((user) => user.email);
            setUserEmail(userEmails);

            if (userEmails.length > 0) {
               const tasksResponse = await TaskService.getTasks();
               const filteredTasks = tasksResponse.data.filter((task) => {
                  const taskUserEmail = task.userEmail[0];
                  return userEmails.includes(taskUserEmail);
               });
               setTasks(filteredTasks);
            }
         } catch (error) {
            console.error("Error fetching data:", error);
         }
      };

      fetchData();
   }, []);

   if (userEmail.length === 0) {
      return <div>Loading...</div>;
   }

   const deleteTask = async (id) => {
      const swalWithBootstrapButtons = Swal.mixin({
         customClass: {
            confirmButton: "btn confirm-button mx-1",
            cancelButton: "btn cancel-button mx-1",
            popup: "my-custom-popup-class",
         },
         buttonsStyling: false,
      });

      try {
         const result = await swalWithBootstrapButtons.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this! This will delete all saved tasks.",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "Just cancel!",
            reverseButtons: true,
         });

         if (result.isConfirmed) {
            await TaskService.deleteTask(id);
            setTasks(tasks.filter((task) => task.id !== id));
            swalWithBootstrapButtons.fire(
               "Cleared!",
               "All tasks have been deleted.",
               "success"
            );
         } else if (result.dismiss === Swal.DismissReason.cancel) {
            swalWithBootstrapButtons.fire(
               "Cancelled",
               "Task is still stored.",
               "info"
            );
         }
      } catch (error) {
         console.error(error);
      }
   };

   const editTask = (id) => {
      navigate(`/update-task/${id}`);
   };

   const stackNow = (id) => {
      navigate(`/stack-task/${id}`);
   };

   const addTask = () => {
      navigate(`/add-task/_add`);
   };

   const comparePredictions = (a, b) => {
      const numericA = parseInt(a.prediction.match(/\d+/), 10);
      const numericB = parseInt(b.prediction.match(/\d+/), 10);

      return numericB - numericA;
   };

   return (
      <>
         <span className="pages-title-predict">Predict Priority Level</span>
         <button onClick={addTask} type="button" className="btn btn-add-task">
            <i className="bx bx-plus"></i> Task
         </button>

         <section className="taskList">
            <div className="custom-table-container mt-4">
               <Table
                  striped
                  hover
                  responsive
                  className={`${isDark ? "table-dark" : "table-light"}`}
               >
                  <thead className="">
                     <tr className={`borderTr ${isDark ? "dark" : ""}`}>
                        <th className="th-weight thead-left">#</th>
                        <th className="th-weight">Task Name</th>
                        <th className="th-weight">Start Date</th>
                        <th className="th-weight">Due Date</th>
                        <th className="th-weight">Priority Lvl</th>
                        <th className="th-weight thead-right">Actions</th>
                     </tr>
                  </thead>
                  <tbody className="my-3">
                     {tasks
                        .filter((task) => !task.isActive)
                        .sort(comparePredictions)
                        .map((task, index) => (
                           <tr key={task.id}>
                              <td className=" ps-3 ">{(numberCount += 1)}</td>
                              <td className="task-name-td">{task.taskName}</td>
                              <td className="start-date-td">{`${
                                 task.recommendation_part1 ===
                                 task.recommendation_part2
                                    ? task.recommendation_part2
                                    : `${task.recommendation_part1} or ${task.recommendation_part2}`
                              }`}</td>
                              <td className="deadline-td">{task.deadline}</td>
                              <td className="deadline-td">{task.prediction}</td>
                              <td className="action">
                                 <button
                                    onClick={() => stackNow(task.id)}
                                    className={`btn rounded-1 start-here-button ${
                                       task.isActive
                                          ? "btn-success disabled"
                                          : "btn-primary"
                                    }`}
                                    disabled={task.isActive}
                                 >
                                    {task.isActive ? "Stacked" : "Stack"}
                                 </button>
                                 <button
                                    onClick={() => editTask(task.id)}
                                    className="btn action-button text-light rounded-1 start-here-button"
                                 >
                                    <i className="bx bx-edit-alt"></i>
                                 </button>
                                 <button
                                    onClick={() => deleteTask(task.id)}
                                    className="btn action-button text-light rounded-1 start-here-button"
                                 >
                                    <i className="bx bx-trash"></i>
                                 </button>
                              </td>
                           </tr>
                        ))}
                     <tr className="white-break-start"></tr>
                  </tbody>
               </Table>
            </div>
         </section>
      </>
   );
};

export default A_StartHere;
