import React from "react";
import C_DashboardDoughnut from "../subComponents/C_DashboardDoughnut";
import C_DashboardBar from "../subComponents/C_DashboardBar";
import C_DashboardTimeTrack from "../subComponents/C_DashboardTimeTrack";

const E_Dashboard = ({ tasks }) => {
   return (
      <div className="chart-main">
         <span className="pages-title-predict-dash ">Dashboard</span>
         <div className="container-fluid g-0 ">
            <div className="row">
               <div className="col text-center">
                  <div>Task Status Distribution</div>
                  <div className=" d-flex justify-content-center align-items-center">
                     <C_DashboardDoughnut tasks={tasks} />
                  </div>
               </div>
               <div className="col text-center">
                  <div>Time Spent</div>
                  <div className="d-flex justify-content-center align-items-center">
                     <C_DashboardTimeTrack tasks={tasks} />
                  </div>
               </div>
            </div>
            <div className="row">
               <div className="col text-center mt-0 mt-md-4">
                  <div>Task Priority Count</div>
                  <div className="d-flex justify-content-center align-items-center ">
                     <C_DashboardBar tasks={tasks} />
                  </div>
               </div>
            </div>
         </div>
      </div>
   );
};

export default E_Dashboard;
