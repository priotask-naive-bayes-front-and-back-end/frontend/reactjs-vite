import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import TaskService from "../services/TaskService";
import Swal from "sweetalert2";

const B_StackBoard = ({ setTasks, tasks, isDark }) => {
   const navigate = useNavigate();

   useEffect(() => {
      const fetchData = async () => {
         try {
            const onlineUsersResponse = await TaskService.getOnlineUsers();
            const onlineUsers = onlineUsersResponse.data.filter(
               (user) => user.email
            );
            const userEmails = onlineUsers.map((user) => user.email);
            setTasks(userEmails);

            if (userEmails.length > 0) {
               const tasksResponse = await TaskService.getTasks();
               const filteredTasks = tasksResponse.data.filter((task) => {
                  const taskUserEmail = task.userEmail[0];
                  return userEmails.includes(taskUserEmail);
               });
               setTasks(filteredTasks);
            }
         } catch (error) {
            console.error("Error fetching data:", error);
         }
      };

      fetchData();
   }, []);

   const deleteTask = async (id) => {
      const swalWithBootstrapButtons = Swal.mixin({
         customClass: {
            confirmButton: "btn confirm-button mx-1",
            cancelButton: "btn cancel-button mx-1",
            popup: "my-custom-popup-class",
         },
         buttonsStyling: false,
      });

      try {
         const result = await swalWithBootstrapButtons.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this! This will delete all saved tasks.",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "Just cancel!",
            reverseButtons: true,
         });

         if (result.isConfirmed) {
            await TaskService.deleteTask(id);
            setTasks(tasks.filter((task) => task.id !== id));
            swalWithBootstrapButtons.fire(
               "Cleared!",
               "All tasks has been deleted.",
               "success"
            );
         } else if (result.dismiss === Swal.DismissReason.cancel) {
            swalWithBootstrapButtons.fire(
               "Cancelled",
               "Task is still stored.",
               "info"
            );
         }
      } catch (error) {
         console.error(error);
      }
   };

   const editTask = (id) => {
      navigate(`/edit-stack-task/${id}`);
   };

   const convertToInProgress = (id) => {
      navigate(`/todo-stacked-data/${id}`);
   };
   const convertToCompleted = (id) => {
      navigate(`/progress-stacked-data/${id}`);
   };

   function calculateDaysDifference(deadline) {
      const today = new Date();
      const parsedDeadline = new Date(deadline);

      today.setHours(0, 0, 0, 0);
      parsedDeadline.setHours(0, 0, 0, 0);
      const timeDifference = parsedDeadline.getTime() - today.getTime();
      const daysDifference = Math.ceil(timeDifference / (1000 * 3600 * 24));

      if (daysDifference === 0) {
         return "🔴Today is the Deadline";
      } else if (daysDifference === -1) {
         return `🚩1 day overdue`;
      } else if (daysDifference < -1) {
         return `🚩${-daysDifference} days overdue`;
      } else if (daysDifference === 1) {
         return `🟡Due tomorrow`;
      } else {
         return `🟢${daysDifference} days left`;
      }
   }

   function daysDifference(dateStarted, isFinished) {
      const parsedDateStarted = new Date(dateStarted);
      const parsedIsFinished = new Date(isFinished);

      parsedDateStarted.setHours(0, 0, 0, 0);
      parsedIsFinished.setHours(0, 0, 0, 0);

      const timeDifference =
         parsedIsFinished.getTime() - parsedDateStarted.getTime();
      const daysDifference = Math.floor(timeDifference / (1000 * 60 * 60 * 24));

      if (daysDifference === 0) {
         return `Just started today`;
      } else if (daysDifference === 1) {
         return `Started yesterday`;
      } else if (daysDifference > 0) {
         return `Started ${daysDifference} days ago`;
      } else {
         return `Start date is after finish date`;
      }
   }

   const dateToday = new Date();
   return (
      <>
         <span className="pages-title-predict-stack ">Stackboard</span>
         <div className={`container main-stack-board `}>
            <div className="row">
               <div className="col-12 col-md-4 col-lg-4">
                  <div
                     className={`box-stack-todo text-center mt-lg-0 ${
                        isDark ? "dark" : ""
                     }`}
                  >
                     To do
                  </div>

                  <div className="stack-container">
                     {tasks.map((task) => (
                        <React.Fragment key={task.id}>
                           {task.isTodo ? (
                              <>
                                 <div
                                    className={`card container g-0 my-2 ${
                                       isDark ? "dark" : ""
                                    }`}
                                 >
                                    <div className="card-body">
                                       <h6 className="card-title py-3 px-3">
                                          {task.taskName}
                                       </h6>
                                       <div className="card-subtitle my-3  me-auto">
                                          <span>
                                             <strong>Due : </strong>
                                          </span>
                                          {`${task.deadline}`}
                                          {task.deadline && (
                                             <span className="days-difference">
                                                {` (${calculateDaysDifference(
                                                   task.deadline
                                                )})`}
                                             </span>
                                          )}
                                       </div>
                                       <div className="card-subtitle mb-2  me-auto my-3">
                                          <div className="recommendation-start">
                                             <span>
                                                <strong>
                                                   Recommended Start:
                                                </strong>
                                             </span>
                                          </div>
                                          {`${
                                             task.recommendation_part1 ===
                                             task.recommendation_part2
                                                ? `${task.recommendation_part2}`
                                                : `${task.recommendation_part1} or ${task.recommendation_part2}`
                                          }`}
                                       </div>
                                       <div className="d-flex ">
                                          <div className="card-subtitle me-auto">
                                             {` ${task.prediction}`}
                                          </div>
                                          <div className="bx-stack-icons ms-auto ">
                                             <span
                                                onClick={() =>
                                                   convertToInProgress(task.id)
                                                }
                                                style={{ cursor: "pointer" }}
                                                className="material-symbols-outlined mx-1 "
                                             >
                                                start
                                             </span>

                                             <span
                                                onClick={() =>
                                                   editTask(task.id)
                                                }
                                                style={{ cursor: "pointer" }}
                                                className="material-symbols-outlined mx-1"
                                             >
                                                edit_square
                                             </span>

                                             <span
                                                onClick={() =>
                                                   deleteTask(task.id)
                                                }
                                                style={{ cursor: "pointer" }}
                                                className="material-symbols-outlined "
                                             >
                                                delete
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </>
                           ) : null}
                        </React.Fragment>
                     ))}
                     <div className="blank-space"></div>
                  </div>
               </div>
               <div className="col-12 col-md-4 col-lg-4 ">
                  <div
                     className={`box-stack-inprogress text-center mt-4 mt-lg-0 ${
                        isDark ? "dark" : ""
                     }`}
                  >
                     In progress
                  </div>
                  <div className="stack-container">
                     {tasks.map((task) => (
                        <React.Fragment key={task.id}>
                           {task.isProgress ? (
                              <>
                                 <div
                                    className={`card container g-0 my-2 ${
                                       isDark ? "dark" : ""
                                    }`}
                                 >
                                    <div className="card-body">
                                       <h6 className="card-title py-3 px-3">
                                          {task.taskName}
                                       </h6>
                                       <div className="card-subtitle my-3 me-auto">
                                          <strong>Due : </strong>
                                          {`${task.deadline}`} <br />
                                          {` (${calculateDaysDifference(
                                             task.deadline
                                          )})`}
                                       </div>
                                       <div className="d-flex">
                                          <div className="card-subtitle my-3  me-auto">
                                             {`${task.prediction}`}
                                          </div>
                                          <div className="bx-stack-icons ms-auto my-3">
                                             <span
                                                className="material-symbols-outlined my-3"
                                                onClick={() =>
                                                   convertToCompleted(task.id)
                                                }
                                                style={{ cursor: "pointer" }}
                                             >
                                                stop_circle
                                             </span>
                                             <span
                                                onClick={() =>
                                                   deleteTask(task.id)
                                                }
                                                style={{ cursor: "pointer" }}
                                                className="material-symbols-outlined"
                                             >
                                                delete
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </>
                           ) : null}
                        </React.Fragment>
                     ))}
                     <div className="blank-space"></div>
                  </div>
               </div>
               <div className="col-12 col-md-4 col-lg-4 ">
                  <div
                     className={`box-stack-completed text-center mt-4 mt-lg-0 ${
                        isDark ? "dark" : ""
                     }`}
                  >
                     Completed
                  </div>
                  <div className="stack-container">
                     {" "}
                     {tasks.map((task) => (
                        <React.Fragment key={task.id}>
                           {task.isCompleted ? (
                              <>
                                 <div
                                    className={`card container g-0 my-2 ${
                                       isDark ? "dark" : ""
                                    }`}
                                 >
                                    <div className="card-body isCompleted">
                                       <h6 className="card-title py-3 px-3 ">
                                          {task.taskName}
                                       </h6>
                                       <div className="card-subtitle my-4 me-auto d-flex">
                                          <span className="material-symbols-outlined">
                                             task
                                          </span>
                                          <span className="white-space"></span>
                                          <div className="align-middle fs-6">
                                             {`Finished at ${task.isFinished}`}
                                          </div>
                                       </div>
                                       <div className="d-flex">
                                          <div className="card-subtitle mb-2  me-auto">
                                             {`${daysDifference(
                                                task.dateStarted,
                                                dateToday
                                             )} `}
                                             <br />
                                             {`(${task.dateStarted})`}
                                          </div>
                                          <div className="bx-stack-icons ms-auto">
                                             <span
                                                onClick={() =>
                                                   deleteTask(task.id)
                                                }
                                                style={{ cursor: "pointer" }}
                                                className="material-symbols-outlined my-3"
                                             >
                                                delete_forever
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </>
                           ) : null}
                        </React.Fragment>
                     ))}
                     <div className="blank-space"></div>
                  </div>
               </div>
            </div>
         </div>
      </>
   );
};

export default B_StackBoard;
