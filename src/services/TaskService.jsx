import axios from "axios";
const TASK_API_BASE_URL = "http://127.0.0.1:5000/tasks";

class TaskService {
   getOnlineUsers() {
      return axios.get(`http://127.0.0.1:5000/online_users`);
   }

   getLogoutUsers() {
      return axios.get("http://127.0.0.1:5000/logout");
   }

   getTasks() {
      return axios.get(TASK_API_BASE_URL);
   }

   createTask(task) {
      return axios.post(TASK_API_BASE_URL, task);
   }

   getTaskById(taskId) {
      return axios.get(`${TASK_API_BASE_URL}/${taskId}`);
   }

   editTask(taskId, updatedTask) {
      return axios.put(`${TASK_API_BASE_URL}/${taskId}`, updatedTask);
   }

   stackNow(taskId, updatedTask) {
      return axios.put(`${TASK_API_BASE_URL}/stack/${taskId}`, updatedTask);
   }

   convertToInProgress(taskId, updatedTask) {
      return axios.put(`${TASK_API_BASE_URL}/isTodo/${taskId}`, updatedTask);
   }

   convertToCompleted(taskId, updatedTask) {
      return axios.put(
         `${TASK_API_BASE_URL}/isProgress/${taskId}`,
         updatedTask
      );
   }

   convertToOverdue(taskId, updatedTask) {
      return axios.put(`${TASK_API_BASE_URL}/isOverdue/${taskId}`, updatedTask);
   }

   deleteTask(taskId) {
      return axios.delete(`${TASK_API_BASE_URL}/${taskId}`);
   }
}

export default new TaskService();
