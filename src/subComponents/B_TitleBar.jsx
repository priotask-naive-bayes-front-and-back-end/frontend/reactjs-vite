import React, { useState } from "react";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import axios from "axios";
import Swal from "sweetalert2";

function B_TitleBar({ toggleSidebar, makeDark, isDark, makeLight }) {
   const [logoutUsers, setLogoutUsers] = useState([]);

   const handleLogout = async () => {
      Swal.fire({
         title: "Are you sure you want to logout?",
         icon: "warning",
         showCancelButton: true,
         confirmButtonText: "Logout",
         cancelButtonText: "Cancel",
      }).then(async (result) => {
         if (result.isConfirmed) {
            window.location.reload();
            try {
               const response = await axios.get(
                  "http://127.0.0.1:5000/logout_users"
               );
               setLogoutUsers(response.data);
               Swal.fire("Logged out successfully!", "", "success");
            } catch (error) {
               console.error("Error fetching logout users:", error);
            }
         }
      });
   };

   return (
      <Navbar
         expand="lg"
         className={`horizontal-navbar-color navbar-horizontal ${
            isDark ? "dark" : ""
         }`}
      >
         <Container className="d-flex">
            <nav className="pt-3 fs-0 align-self-center " href="#home">
               <i
                  onClick={toggleSidebar}
                  className="bx bxs-grid-alt arrow-icon d-md-none d-block"
               ></i>
            </nav>
            <div className="ms-auto mode align-self-center">
               {isDark ? (
                  <i
                     onClick={makeLight}
                     className="bx bx-sun"
                     style={{ cursor: "pointer" }}
                  ></i>
               ) : (
                  <i
                     onClick={makeDark}
                     className="bx bx-moon"
                     style={{ cursor: "pointer" }}
                  ></i>
               )}
               <i
                  onClick={handleLogout}
                  style={{ cursor: "pointer" }}
                  className="bx bx-log-out me-md-3"
               ></i>
            </div>
         </Container>
      </Navbar>
   );
}

export default B_TitleBar;
