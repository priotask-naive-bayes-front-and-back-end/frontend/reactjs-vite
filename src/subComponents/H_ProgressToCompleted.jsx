import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import TaskService from "../services/TaskService";

function H_ProgressToCompleted({ tasks, setFinishedDate }) {
   const navigate = useNavigate();

   const { id: taskId } = useParams();

   const [task, setTask] = useState({
      id: taskId,
      isProgress: "",
      isCompleted: "",
      isFinished: "",
      timeSpent: "",
   });
   function timeDifference(dateStarted, isFinished) {
      const parsedDateStarted = new Date(dateStarted);
      const parsedIsFinished = new Date(isFinished);

      const timeDifferenceInMilliseconds = parsedIsFinished - parsedDateStarted;

      const timeDifferenceInHours = Math.floor(
         timeDifferenceInMilliseconds / (1000 * 60 * 60)
      );
      const timeDifferenceInMinutes = Math.floor(
         (timeDifferenceInMilliseconds / (1000 * 60)) % 60
      );

      return {
         hours: timeDifferenceInHours,
         minutes: timeDifferenceInMinutes,
      };
   }

   useEffect(() => {
      TaskService.getTaskById(taskId).then((res) => {
         // setFinishedDate(formattedDate);
         let taskData = res.data;
         const currentDate = new Date();

         const timeDifferenceObj = timeDifference(
            taskData.dateStarted,
            currentDate.toLocaleString()
         );

         setTask({
            id: taskData.id,
            isProgress: false,
            isCompleted: true,
            isFinished: currentDate.toLocaleString(),
            timeSpent: timeDifferenceObj,
         });
      });
   }, [taskId]);

   const completeNow = (e) => {
      e.preventDefault();
      TaskService.convertToCompleted(taskId, task).then((res) => {
         navigate("/stackboard");
      });
   };

   const cancel = () => {
      navigate("/stackboard");
   };

   return (
      <div>
         <div className="">
            <div className="">
               <div className={`stack-confirmation  modal-body-1`}>
                  <div className="d-flex mt-2 mx-2"></div>
                  <form>
                     <div className="mb-3 ">
                        <div className="form-check form-switch mb-5">
                           <label
                              className="form-check-label"
                              htmlFor="defaultCheck1"
                           >
                              <span>Do you want to start the task?</span>
                           </label>
                        </div>
                     </div>
                  </form>
                  <div className="d-flex">
                     {" "}
                     <div className="m-3 text-end">
                        <button
                           type="button"
                           className="btn btn-secondary mx-2"
                           data-bs-dismiss="modal"
                           onClick={cancel}
                        >
                           No
                        </button>

                        <button
                           className="btn btn-success btn-predict mx-2"
                           aria-label="Predict Priority"
                           onClick={completeNow}
                        >
                           Yes, run it now
                        </button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   );
}

export default H_ProgressToCompleted;
