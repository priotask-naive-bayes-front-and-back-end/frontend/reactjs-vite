import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import TaskService from "../services/TaskService";

const D_MainForm = () => {
   const navigate = useNavigate();
   const { id } = useParams();

   const [taskName, setTaskName] = useState("");
   const [difficulty, setDifficulty] = useState(0);
   const [deadline, setDeadline] = useState("");
   const [timeNeeded, setTimeNeeded] = useState(1);
   const [isActive, setIsActive] = useState(false);
   const [isTodo, setIsTodo] = useState(false);
   const [isProgress, setIsProgress] = useState(false);
   const [isOverdue, setIsOverdue] = useState(false);
   const [isCompleted, setIsCompleted] = useState(false);
   const [isFinished, setIsFinished] = useState(null);
   const [dateStarted, setDateStarted] = useState(null);
   const [archived, setArchived] = useState(false);
   const [timeSpent, setTimeSpent] = useState(null);
   const [userEmail, setUserEmail] = useState(null);

   const onChangeTaskNameHandler = (event) => {
      setTaskName(event.target.value);
   };

   const onChangeDifficultyHandler = (event) => {
      const selectedValue = event.target.value;
      const numericValue = parseInt(selectedValue);

      if (!isNaN(numericValue)) {
         setDifficulty(numericValue);
      }
   };

   // const onChangeDeadlineHandler = (event) => {
   //    setDeadline(event.target.value);
   // };
   // Calculate one year from today
   const oneYearFromNow = new Date();
   oneYearFromNow.setFullYear(oneYearFromNow.getFullYear() + 1);

   const onChangeDeadlineHandler = (event) => {
      const selectedValue = event.target.value;
      const numericValue = Date.parse(selectedValue);

      if (!isNaN(numericValue)) {
         // Ensure the selected date is not earlier than today
         const minDate = new Date().toISOString().split("T")[0];
         // Ensure the selected date is not later than one year from today
         const maxDate = oneYearFromNow.toISOString().split("T")[0];

         // Set the selected date within the specified range
         if (selectedValue >= minDate && selectedValue <= maxDate) {
            setDeadline(selectedValue);
         } else if (selectedValue < minDate) {
            setDeadline(minDate);
         } else {
            setDeadline(maxDate);
         }
      }
   };

   const onChangeTimeNeededHandler = (event) => {
      const selectedValue = event.target.value;
      const numericValue = parseInt(selectedValue);

      if (!isNaN(numericValue)) {
         setTimeNeeded(numericValue);
      }
   };

   const saveOrUpdateTask = (e) => {
      e.preventDefault();

      let task = {
         taskName: taskName,
         difficulty: difficulty,
         deadline: deadline,
         timeNeeded: timeNeeded,
         isActive: isActive,
         isTodo: isTodo,
         isProgress: isProgress,
         isOverdue: isOverdue,
         isCompleted: isCompleted,
         isFinished: isFinished,
         dateStarted: dateStarted,
         archived: archived,
         timeSpent: timeSpent,
         userEmail: userEmail,
      };

      if (id === "_add") {
         TaskService.createTask(task).then((res) => {
            navigate("/start");
         });
      }
   };

   const cancel = () => {
      navigate("/start");
   };

   useEffect(() => {
      TaskService.getOnlineUsers()
         .then((response) => {
            const onlineUsers = response.data.filter((user) => user.email);
            const userEmails = onlineUsers.map((user) => user.email);
            setUserEmail(userEmails);
         })
         .catch((error) => {
            console.error("Error fetching online status:", error);
         });

      if (id !== "_add") {
         TaskService.getTaskById(id).then((res) => {
            let task = res.data;
            setTaskName(task.taskName);
            setDifficulty(task.difficulty);
            setDeadline(task.deadline);
            setTimeNeeded(task.timeNeeded);
            setPriority_level(task.priority_level);
            setIsActive(task.isActive);
            setIsTodo(task.isTodo);
            setIsProgress(task.isProgress);
            setIsOverdue(task.isOverdue);
            setIsCompleted(task.isCompleted);
            setIsFinished(task.isFinished);
            setDateStarted(task.dateStarted);
            setArchived(task.archived);
            setTimeSpent(task.timeSpent);
         });
      }
   }, [id]);

   const renderFormGroup = (labelText, id, children) => (
      <div className="my-4">
         <label htmlFor={id} className="form-label text-light">
            {labelText}
         </label>
         {children}
      </div>
   );

   return (
      <div>
         <div className="">
            <div className="">
               <div className={`form-add-task modal-body-1`}>
                  <div className="d-flex mt-2 mx-2">
                     <h1 className="">
                        <span>
                           <i
                              className={`bx modal-icon text-light fs-1 bxl-steam align-self-center animated-navbar-2`}
                           ></i>
                        </span>
                     </h1>
                  </div>
                  <div className="">
                     <form className="">
                        {renderFormGroup(
                           "Task Name",
                           "task",
                           <input
                              type="text"
                              id="task"
                              className="form-control"
                              value={taskName}
                              onChange={onChangeTaskNameHandler}
                              required
                              autoFocus
                              maxLength={80}
                              placeholder="Enter new task"
                           />
                        )}

                        {renderFormGroup(
                           "Difficulty",
                           "difficulty",
                           <select
                              id="difficulty"
                              className="form-select"
                              aria-label="Select difficulty level"
                              value={difficulty}
                              onChange={onChangeDifficultyHandler}
                           >
                              <option value="0">Easy</option>
                              <option value="1">Moderate</option>
                              <option value="2">Hard</option>
                           </select>
                        )}

                        {/* {renderFormGroup(
                           "Deadline",
                           "deadline",
                           <input
                              id="deadline"
                              className="date form-control"
                              type="date"
                              value={deadline}
                              onChange={onChangeDeadlineHandler}
                           />
                        )} */}
                        {renderFormGroup(
                           "Deadline",
                           "deadline",
                           <input
                              id="deadline"
                              className="date form-control"
                              type="date"
                              value={deadline}
                              required
                              onChange={onChangeDeadlineHandler}
                              min={new Date().toISOString().split("T")[0]} // Minimum date is today
                              max={oneYearFromNow.toISOString().split("T")[0]} // Maximum date is one year from today
                           />
                        )}

                        {renderFormGroup(
                           "Time Needed for Completion",
                           "needed_time",
                           <div className="scrollable-select">
                              <select
                                 id="needed_time"
                                 className="form-select"
                                 aria-label="Select time needed for completion"
                                 value={timeNeeded}
                                 onChange={onChangeTimeNeededHandler}
                              >
                                 <option value="1">1 hour</option>
                                 <option value="2">2 hours</option>
                                 <option value="3">3 hours</option>
                                 <option value="4">4 hours</option>
                                 <option value="5">5 hours</option>
                                 <option value="6">6 hours</option>
                                 <option value="7">7 hours</option>
                                 <option value="8">8 hours</option>
                                 <option value="9">9 hours</option>
                                 <option value="10">10 hours</option>
                                 <option value="11">11 hours</option>
                                 <option value="12">12 hours</option>
                                 <option value="13">13 hours</option>
                                 <option value="14">14 hours</option>
                                 <option value="15">15 hours</option>
                                 <option value="16">16 hours</option>
                                 <option value="17">17 hours</option>
                                 <option value="18">18 hours</option>
                                 <option value="19">19 hours</option>
                                 <option value="20">20 hours</option>
                              </select>
                           </div>
                        )}
                     </form>
                  </div>
                  <div className="m-3">
                     <button
                        type="button"
                        className="btn btn-secondary mx-2"
                        data-bs-dismiss="modal"
                        onClick={cancel}
                     >
                        Close
                     </button>
                     <button
                        className="btn btn-success btn-predict mx-2"
                        aria-label="Predict Priority"
                        onClick={saveOrUpdateTask}
                        disabled={!taskName || !deadline}
                     >
                        Predict
                     </button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   );
};

export default D_MainForm;
