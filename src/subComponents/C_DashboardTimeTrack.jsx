import React, { useEffect, useRef } from "react";
import Chart from "chart.js/auto";

const C_DashboardDoughnut = ({ tasks }) => {
   const chartRef = useRef(null);

   useEffect(() => {
      const completedTasks = tasks.filter((task) => task.isCompleted);

      let lowTime = 0;
      let moderateTime = 0;
      let highTime = 0;
      let extremelyHighTime = 0;

      completedTasks.forEach((task) => {
         if (task.prediction) {
            if (task.prediction.includes("Low:")) {
               lowTime += task.timeSpent.hours * 60 + task.timeSpent.minutes;
            } else if (task.prediction.includes("Medium:")) {
               moderateTime +=
                  task.timeSpent.hours * 60 + task.timeSpent.minutes;
            } else if (task.prediction.includes("High:")) {
               highTime += task.timeSpent.hours * 60 + task.timeSpent.minutes;
            } else if (task.prediction.includes("Extremely High :")) {
               extremelyHighTime +=
                  task.timeSpent.hours * 60 + task.timeSpent.minutes;
            }
         }
      });

      const data = [lowTime, moderateTime, highTime, extremelyHighTime];

      const ctx = document.getElementById("timeSpent").getContext("2d");

      if (chartRef.current) {
         chartRef.current.destroy();
      }

      chartRef.current = new Chart(ctx, {
         type: "pie",
         data: {
            labels: ["Low", "Moderate", "High", "Extremely High"],
            datasets: [
               {
                  label: "Time Spent by Priority Level",
                  data: data,
                  backgroundColor: [
                     "rgb(96, 96, 96)",
                     "rgb(67, 73, 161)",
                     "rgb(227, 141, 30)",
                     "rgb(249, 86, 86)",
                  ],
               },
            ],
         },
         options: {
            plugins: {
               legend: {
                  display: true,
                  position: "bottom",
               },

               tooltip: {
                  callbacks: {
                     label: function (context) {
                        const label = context.label || "";
                        if (label) {
                           const minutes = context.parsed;
                           const hours = Math.floor(minutes / 60);
                           const remainingMinutes = minutes % 60;

                           let formattedTime = "";

                           if (hours > 0) {
                              formattedTime += hours + " hours ";
                           }

                           if (remainingMinutes > 0) {
                              formattedTime += remainingMinutes + " minutes";
                           }

                           return label + ": " + formattedTime;
                        }
                        return "";
                     },
                  },
               },
            },
         },
      });

      return () => {
         if (chartRef.current) {
            chartRef.current.destroy();
         }
      };
   }, [tasks]);

   return (
      <>
         <canvas className="time" id="timeSpent"></canvas>
      </>
   );
};

export default C_DashboardDoughnut;
