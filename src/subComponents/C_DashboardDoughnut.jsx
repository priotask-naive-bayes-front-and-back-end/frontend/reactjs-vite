import React, { useEffect, useRef } from "react";
import Chart from "chart.js/auto";

const C_DashboardDoughnut = ({ tasks }) => {
   const chartRef = useRef(null);

   useEffect(() => {
      const totalTasks = tasks.length;

      const todoCount = tasks.filter((task) => task.isTodo).length;
      const progressCount = tasks.filter((task) => task.isProgress).length;
      const completedCount = tasks.filter((task) => task.isCompleted).length;

      const todoPercentage = (todoCount / totalTasks) * 100;
      const progressPercentage = (progressCount / totalTasks) * 100;
      const completedPercentage = (completedCount / totalTasks) * 100;

      const data = [todoPercentage, progressPercentage, completedPercentage];

      const ctx = document.getElementById("tasksChart").getContext("2d");

      if (chartRef.current) {
         chartRef.current.destroy();
      }

      chartRef.current = new Chart(ctx, {
         type: "doughnut",
         data: {
            labels: ["Todo", "Progress", "Completed"],
            datasets: [
               {
                  label: "Task Counts",
                  data: data,
                  backgroundColor: [
                     "rgb(255, 99, 133)",
                     "rgb(0, 198, 145)",
                     "rgb(54, 163, 235)",
                  ],
                  borderColor: [
                     "rgb(255, 99, 133)",
                     "rgb(2, 143, 105)",
                     "rgb(54, 163, 235)",
                  ],
                  borderWidth: 1,
               },
            ],
         },
         options: {
            plugins: {
               legend: {
                  display: true,
                  position: "bottom",
               },
               tooltip: {
                  callbacks: {
                     label: function (context) {
                        const label = context.label || "";
                        if (label) {
                           return (
                              label + ": " + context.parsed.toFixed(2) + "%"
                           );
                        }
                        return "";
                     },
                  },
               },
            },
         },
      });

      return () => {
         if (chartRef.current) {
            chartRef.current.destroy();
         }
      };
   }, [tasks]);

   return <canvas className="chart" id="tasksChart"></canvas>;
};

export default C_DashboardDoughnut;
