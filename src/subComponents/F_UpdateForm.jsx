import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import TaskService from "../services/TaskService";

function H_TodoUpdateForm() {
   const navigate = useNavigate();
   const { id: taskId } = useParams();

   const [task, setTask] = useState({
      id: taskId,
      taskName: "",
      difficulty: "",
      deadline: "",
      timeNeeded: "",
      isActive: "",
      prediction: "",
      recommendation_part1: "",
      recommendation_part2: "",
   });

   useEffect(() => {
      TaskService.getTaskById(taskId).then((res) => {
         let taskData = res.data;
         setTask({
            id: taskData.id,
            taskName: taskData.taskName,
            difficulty: taskData.difficulty,
            deadline: taskData.deadline,
            timeNeeded: taskData.timeNeeded,
            isActive: taskData.isActive,
            prediction: taskData.prediction,
            recommendation_part1: taskData.recommendation_part1,
            recommendation_part2: taskData.recommendation_part2,
         });
      });
   }, [taskId]);

   const changeTaskNameHandler = (event) => {
      setTask((prevTask) => ({
         ...prevTask,
         taskName: event.target.value,
      }));
   };

   const changeDifficultyHandler = (event) => {
      setTask((prevTask) => ({
         ...prevTask,
         difficulty: event.target.value,
      }));
   };

   const changeDeadlineHandler = (event) => {
      setTask((prevTask) => ({
         ...prevTask,
         deadline: event.target.value,
      }));
   };

   const changeTimeNeededHandler = (event) => {
      setTask((prevTask) => ({
         ...prevTask,
         timeNeeded: event.target.value,
      }));
   };

   const editTask = (e) => {
      e.preventDefault();
      TaskService.editTask(taskId, task).then((res) => {
         navigate("/tasks");
      });
   };

   const cancel = () => {
      navigate("/tasks");
   };

   const renderFormGroup = (labelText, id, children) => (
      <div className="my-4">
         <label htmlFor={id} className="form-label text-light">
            {labelText}
         </label>
         {children}
      </div>
   );
   return (
      <div>
         <div className="">
            <div className="">
               <div className={`form-add-task update-add-task modal-body`}>
                  <div className="d-flex mt-2 mx-2">
                     <h1 className="">
                        <span>
                           <i
                              className={`bx modal-icon text-light fs-1 bx-git-commit align-self-center animated-navbar-2`}
                           ></i>
                        </span>
                     </h1>
                  </div>
                  <div className="">
                     <form className="">
                        {renderFormGroup(
                           "Task Name",
                           "task",
                           <input
                              type="text"
                              id="task"
                              className="form-control"
                              value={task.taskName}
                              onChange={changeTaskNameHandler}
                              required
                              autoFocus
                              maxLength={80}
                              placeholder="Enter new task"
                           />
                        )}

                        {renderFormGroup(
                           "Difficulty",
                           "difficulty",
                           <select
                              id="difficulty"
                              className="form-select"
                              aria-label="Select difficulty level"
                              value={task.difficulty}
                              onChange={changeDifficultyHandler}
                           >
                              <option value="0">Easy</option>
                              <option value="1">Moderate</option>
                              <option value="2">Hard</option>
                           </select>
                        )}

                        {renderFormGroup(
                           "Deadline",
                           "deadline",
                           <input
                              id="deadline"
                              className="date form-control"
                              type="date"
                              value={task.deadline}
                              onChange={changeDeadlineHandler}
                           />
                        )}

                        {renderFormGroup(
                           "Time Needed for Completion",
                           "needed_time",
                           <div className="scrollable-select">
                              <select
                                 id="needed_time"
                                 className="form-select"
                                 aria-label="Select time needed for completion"
                                 value={task.timeNeeded}
                                 onChange={changeTimeNeededHandler}
                              >
                                 <option value="1">1 hour</option>
                                 <option value="2">2 hours</option>
                                 <option value="3">3 hours</option>
                                 <option value="4">4 hours</option>
                                 <option value="5">5 hours</option>
                                 <option value="6">6 hours</option>
                                 <option value="7">7 hours</option>
                                 <option value="8">8 hours</option>
                                 <option value="9">9 hours</option>
                                 <option value="10">10 hours</option>
                                 <option value="11">11 hours</option>
                                 <option value="12">12 hours</option>
                                 <option value="13">13 hours</option>
                                 <option value="14">14 hours</option>
                                 <option value="15">15 hours</option>
                                 <option value="16">16 hours</option>
                                 <option value="17">17 hours</option>
                                 <option value="18">18 hours</option>
                                 <option value="19">19 hours</option>
                                 <option value="20">20 hours</option>
                              </select>
                           </div>
                        )}
                     </form>
                  </div>
                  <div className="m-3">
                     <button
                        type="button"
                        className="btn btn-secondary mx-2"
                        data-bs-dismiss="modal"
                        onClick={cancel}
                     >
                        Close
                     </button>
                     <button
                        className="btn btn-success btn-predict mx-2"
                        aria-label="Predict Priority"
                        onClick={editTask}
                     >
                        Save
                     </button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   );
}

export default H_TodoUpdateForm;
