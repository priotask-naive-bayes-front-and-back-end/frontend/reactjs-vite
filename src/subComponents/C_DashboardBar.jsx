import React, { useEffect, useRef } from "react";
import Chart from "chart.js/auto";

const C_DashboardBar = ({ tasks }) => {
   const chartRef = useRef(null);

   useEffect(() => {
      const extremelyPriorityCount = tasks.filter((task) =>
         task.prediction.includes("Extremely High :")
      ).length;
      const highPriorityCount = tasks.filter((task) =>
         task.prediction.includes("High:")
      ).length;
      const mediumPriorityCount = tasks.filter((task) =>
         task.prediction.includes("Medium")
      ).length;
      const lowPriorityCount = tasks.filter((task) =>
         task.prediction.includes("Low")
      ).length;

      const data = [
         lowPriorityCount,
         mediumPriorityCount,
         highPriorityCount,
         extremelyPriorityCount,
      ];

      const ctx = document.getElementById("priorityChart").getContext("2d");

      if (chartRef.current) {
         chartRef.current.destroy();
      }

      chartRef.current = new Chart(ctx, {
         type: "bar",
         data: {
            labels: [
               "Low Priority",
               "Medium Priority",
               "High Priority",
               "Extremely Priority",
            ],
            datasets: [
               {
                  label: "Task Counts",
                  data: data,
                  backgroundColor: [
                     "rgb(18, 56, 56)",
                     "rgb(29, 107, 107)",
                     "rgb(35, 171, 171)",
                     "rgb(11, 219, 219)",
                  ],
               },
            ],
         },
         options: {
            scales: {
               y: {
                  beginAtZero: true,
               },
            },
            plugins: {
               legend: {
                  display: false,
               },
            },
         },
      });

      return () => {
         if (chartRef.current) {
            chartRef.current.destroy();
         }
      };
   }, [tasks]);

   return <canvas className="chart-bar" id="priorityChart"></canvas>;
};

export default C_DashboardBar;
