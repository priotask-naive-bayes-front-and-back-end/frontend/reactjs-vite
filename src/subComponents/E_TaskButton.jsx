import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import TaskService from "../services/TaskService";

function E_TaskButton({ tasks }) {
   const navigate = useNavigate();
   const { id: taskId } = useParams();

   const [task, setTask] = useState({
      id: taskId,
      isActive: "",
      isTodo: "",
   });

   useEffect(() => {
      TaskService.getTaskById(taskId).then((res) => {
         let taskData = res.data;
         setTask({
            id: taskData.id,
            isActive: true,
            isTodo: true,
         });
      });
   }, [taskId]);

   const stackNow = (e) => {
      e.preventDefault();
      TaskService.stackNow(taskId, task).then((res) => {
         navigate("/start");
      });
   };

   const cancel = () => {
      navigate("/start");
   };

   return (
      <div>
         <div className="">
            <div className="">
               <div className={`stack-confirmation  modal-body-1`}>
                  <div className="d-flex mt-2 mx-2"></div>
                  <form>
                     <div className="mb-3 ">
                        <div className="form-check form-switch mb-5">
                           <label
                              className="form-check-label"
                              htmlFor="defaultCheck1"
                           >
                              <span>
                                 Are you sure you wish to transfer the task to
                                 Stackboard?
                              </span>
                           </label>
                        </div>
                     </div>
                  </form>
                  <div className="d-flex">
                     {" "}
                     <div className="m-3 text-end">
                        <button
                           type="button"
                           className="btn btn-secondary mx-2"
                           data-bs-dismiss="modal"
                           onClick={cancel}
                        >
                           No
                        </button>

                        <button
                           className="btn btn-success btn-predict mx-2"
                           aria-label="Predict Priority"
                           onClick={stackNow}
                        >
                           Yes, Stack now.
                        </button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   );
}

export default E_TaskButton;
