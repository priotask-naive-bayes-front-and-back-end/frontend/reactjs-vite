import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import TaskService from "../services/TaskService";

function E_TaskButton({ tasks }) {
   const navigate = useNavigate();
   const { id: taskId } = useParams();

   const [task, setTask] = useState({
      id: taskId,
      isTodo: "",
      isProgress: "",
      dateStarted: "",
   });

   useEffect(() => {
      TaskService.getTaskById(taskId).then((res) => {
         let taskData = res.data;
         const currentDate = new Date();
         setTask({
            id: taskData.id,
            isTodo: false,
            isProgress: true,
            dateStarted: currentDate.toLocaleString(),
         });
      });
   }, [taskId]);

   const stackNow = (e) => {
      e.preventDefault();
      TaskService.convertToInProgress(taskId, task).then((res) => {
         navigate("/stackboard");
      });
   };

   const cancel = () => {
      navigate("/stackboard");
   };

   return (
      <div>
         <div className="">
            <div className="">
               <div className={`stack-confirmation  modal-body-1`}>
                  <div className="d-flex mt-2 mx-2"></div>
                  <form>
                     <div className="mb-3 ">
                        <div className="form-check form-switch mb-5">
                           <label
                              className="form-check-label"
                              htmlFor="defaultCheck1"
                           >
                              <span>Do you want to start the task?</span>
                           </label>
                        </div>
                     </div>
                  </form>
                  <div className="d-flex">
                     {" "}
                     <div className="m-3 text-end">
                        <button
                           type="button"
                           className="btn btn-secondary mx-2"
                           data-bs-dismiss="modal"
                           onClick={cancel}
                        >
                           No
                        </button>

                        <button
                           className="btn btn-success btn-predict mx-2"
                           aria-label="Predict Priority"
                           onClick={stackNow}
                        >
                           Yes, run it now
                        </button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   );
}

export default E_TaskButton;
