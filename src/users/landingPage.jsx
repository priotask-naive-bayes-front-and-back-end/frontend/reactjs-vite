import React from "react";

import { Link } from "react-router-dom";

export default function LandingPage() {
   return (
      <div>
         <div className="container h-100">
            <div className="row h-100">
               <div className="col-12 login-register">
                  <div className="welcome">
                     <h1>
                        Welcome to
                        <span className="priotask-Name">
                           <strong> PrioTask</strong>
                        </span>
                     </h1>
                     <p>
                        <Link to="/login" className="btn btn-primary">
                           Login
                        </Link>{" "}
                        |
                        <Link to="/register" className="btn btn-secondart">
                           Register
                        </Link>
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   );
}
