import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export default function RegisterPage() {
   const [email, setEmail] = useState("");
   const [password, setPassword] = useState("");

   const navigate = useNavigate();

   const registerUser = () => {
      axios
         .post("http://127.0.0.1:5000/signup", {
            email: email,
            password: password,
         })
         .then(function (response) {
            console.log(response);
            navigate("/");
         })
         .catch(function (error) {
            console.log(error, "error");
            if (error.response.status === 401) {
               alert("Invalid credentials");
            }
         });
   };

   return (
      <div>
         <div className="container  register-page">
            <div className="container-fluid h-custom">
               <div>
                  <div>
                     <form>
                        <div>
                           <p className="lead fw-normal mb-0 me-3">
                              Create Your Account
                           </p>
                           <br />
                        </div>

                        <div className="form-outline mb-4">
                           <input
                              type="email"
                              value={email}
                              onChange={(e) => setEmail(e.target.value)}
                              id="form3Example3"
                              className="form-control form-control-lg"
                              placeholder="Enter a valid email address"
                           />
                           <label className="form-label" for="form3Example3">
                              Email address
                           </label>
                        </div>

                        <div className="form-outline mb-3">
                           <input
                              type="password"
                              value={password}
                              onChange={(e) => setPassword(e.target.value)}
                              id="form3Example4"
                              className="form-control form-control-lg"
                              placeholder="Enter password"
                           />
                           <label className="form-label" for="form3Example4">
                              Password
                           </label>
                        </div>

                        <div className="text-center text-lg-start mt-4 pt-2">
                           <button
                              type="button"
                              className="btn btn-primary"
                              onClick={() => registerUser()}
                           >
                              Sign Up
                           </button>
                           <p className="small fw-bold mt-2 pt-1 mb-0">
                              Login to your account{" "}
                              <a href="/login" className="link-danger">
                                 Login
                              </a>
                           </p>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   );
}
