import React, { useState, useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import A_StartHere from "./components/A_StartHere";
import B_StackBoard from "./components/B_StackBoard";
import D_Calendar from "./components/D_Calendar";
import E_Dashboard from "./components/E_Dashboard";
import D_MainForm from "./subComponents/D_MainForm";
import F_UpdateForm from "./subComponents/F_UpdateForm";
import E_TaskButton from "./subComponents/E_TaskButton";
import I_TodoUpdateForm from "./subComponents/I_TodoUpdateForm";
import G_TodoToInProgress from "./subComponents/G_TodoToInProgress";
import H_ProgressToCompleted from "./subComponents/H_ProgressToCompleted";
import A_VerticalNavbar from "./subComponents/A_VerticalNavbar";
import B_TitleBar from "./subComponents/B_TitleBar";
import TaskService from "./services/TaskService";
import LandingPage from "./users/landingPage";
import LoginPage from "./users/login";
import RegisterPage from "./users/register";

function App() {
   const [sidebarHidden, setSidebarHidden] = useState(false);

   const [isDark, setIsDark] = useState(false);
   const [tasks, setTasks] = useState([]);
   const [online, setOnline] = useState(null);
   const [refreshed, setRefreshed] = useState(false); // Track if the page has been refreshed

   useEffect(() => {
      TaskService.getOnlineUsers()
         .then((response) => {
            const isAnyUserOnline = response.data.some((user) => user.isOnline);
            setOnline(isAnyUserOnline);
            setRefreshed(true);
         })
         .catch((error) => {
            console.error("Error fetching online status:", error);
            setOnline(false);
         });

      TaskService.getTasks()
         .then((response) => {
            setTasks(response.data);
         })
         .catch((error) => {
            console.error("Error fetching tasks:", error);
         });
   }, []);
   console.log(refreshed);

   const toggleSidebar = () => {
      setSidebarHidden(!sidebarHidden);
   };

   const makeDark = () => {
      setIsDark(true);
   };
   const makeLight = () => {
      setIsDark(false);
   };

   return (
      <>
         <div className={` ${isDark ? "superText" : ""}`}>
            <BrowserRouter>
               {online ? (
                  <div className="container-fluid g-0 d-flex main-container">
                     <A_VerticalNavbar
                        makeLight={makeLight}
                        makeDark={makeDark}
                        isDark={isDark}
                        toggleSidebar={toggleSidebar}
                        sidebarHidden={sidebarHidden}
                        setSidebarHidden={setSidebarHidden}
                     />
                     <div className="container-fluid g-0 d-block">
                        <div className="">
                           <div>
                              <B_TitleBar
                                 isDark={isDark}
                                 makeDark={makeDark}
                                 toggleSidebar={toggleSidebar}
                                 sidebarHidden={sidebarHidden}
                                 setSidebarHidden={setSidebarHidden}
                                 makeLight={makeLight}
                              />
                           </div>

                           <div className="routes">
                              <Routes>
                                 <Route
                                    path="/start"
                                    element={
                                       <A_StartHere
                                          isDark={isDark}
                                          makeLight={makeLight}
                                          setTasks={setTasks}
                                          tasks={tasks}
                                       />
                                    }
                                 ></Route>
                                 <Route
                                    path="/update-task/:id"
                                    element={
                                       <F_UpdateForm
                                          isDark={isDark}
                                          makeLight={makeLight}
                                       />
                                    }
                                 ></Route>
                                 <Route
                                    path="/add-task/:id"
                                    element={
                                       <D_MainForm
                                          isDark={isDark}
                                          makeLight={makeLight}
                                       />
                                    }
                                 ></Route>
                                 <Route
                                    path="/stack-task/:id"
                                    element={
                                       <E_TaskButton
                                          isDark={isDark}
                                          makeLight={makeLight}
                                          tasks={tasks}
                                       />
                                    }
                                 ></Route>

                                 <Route
                                    path="/stackboard"
                                    element={
                                       <B_StackBoard
                                          isDark={isDark}
                                          makeLight={makeLight}
                                          setTasks={setTasks}
                                          tasks={tasks}
                                       />
                                    }
                                 ></Route>
                                 <Route
                                    path="/edit-stack-task/:id"
                                    element={
                                       <I_TodoUpdateForm
                                          isDark={isDark}
                                          makeLight={makeLight}
                                          tasks={tasks}
                                       />
                                    }
                                 ></Route>
                                 <Route
                                    path="/todo-stacked-data/:id"
                                    element={
                                       <G_TodoToInProgress
                                          isDark={isDark}
                                          makeLight={makeLight}
                                          tasks={tasks}
                                       />
                                    }
                                 ></Route>
                                 <Route
                                    path="/progress-stacked-data/:id"
                                    element={
                                       <H_ProgressToCompleted
                                          isDark={isDark}
                                          makeLight={makeLight}
                                          tasks={tasks}
                                       />
                                    }
                                 ></Route>
                                 <Route
                                    path="/dashboard"
                                    element={
                                       <E_Dashboard
                                          isDark={isDark}
                                          makeLight={makeLight}
                                          tasks={tasks}
                                       />
                                    }
                                 ></Route>
                                 <Route
                                    path="/calendar"
                                    element={
                                       <D_Calendar
                                          isDark={isDark}
                                          makeLight={makeLight}
                                          tasks={tasks}
                                       />
                                    }
                                 ></Route>
                              </Routes>
                           </div>
                        </div>
                     </div>
                  </div>
               ) : (
                  <>
                     <div className="landing-page">
                        <LandingPage />
                     </div>
                     <Routes>
                        <Route
                           path="/login"
                           element={
                              <LoginPage
                                 isDark={isDark}
                                 makeLight={makeLight}
                                 setTasks={setTasks}
                                 tasks={tasks}
                              />
                           }
                        ></Route>
                        <Route
                           path="/register"
                           element={
                              <RegisterPage
                                 isDark={isDark}
                                 makeLight={makeLight}
                                 setTasks={setTasks}
                                 tasks={tasks}
                              />
                           }
                        ></Route>
                     </Routes>
                  </>
               )}
            </BrowserRouter>
            <div
               className={`view-page-overlay-blur ${isDark ? "dark" : ""}`}
            ></div>

            <div className="view-page">
               <video autoPlay loop muted className="video">
                  <source
                     src="https://res.cloudinary.com/dv3x7qoak/video/upload/v1692071694/aaaa_ad9tya.mp4"
                     type="video/mp4"
                  />
               </video>
            </div>
         </div>
      </>
   );
}

export default App;
